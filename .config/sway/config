# Read `man 5 sway` for a complete reference.

### Variables
set $cl_high #b4befe
set $cl_indi #9399b2
set $cl_back #1e1e2e
set $cl_fore #cdd6f4
set $cl_urge #f38ba8

set $mod Mod1
set $term alacritty
set $menu dmenu_path | dmenu -fn 'Hack:size=11' -h 24 -nb \\$cl_back -nf \\$cl_fore -sb \\$cl_high -sf \\$cl_back | xargs swaymsg exec --
set $desktop_menu wofi -i -Ddrun-print_command=true --show drun | xargs swaymsg exec --
swaynag_command swaynagmode

# Keyboard controls in swaynag
set $nag exec swaynagmode
mode "nag" {
  bindsym {
    Ctrl+d    mode "default"

    Ctrl+c    $nag --exit
    q         $nag --exit
    Escape    $nag --exit

    Return    $nag --confirm

    #Tab       $nag --select prev
    #Shift+Tab $nag --select next

    Left      $nag --select next
    Right     $nag --select prev

    #Up        $nag --select next
    #Down      $nag --select prev
  }
}

### Output configuration

output "Beihai Century Joint Innovation Technology Co.,Ltd GB-2461CF             " mode 1920x1080@144Hz adaptive_sync off
output * bg $cl_back solid_color

# Lock screen after 5 minutes
exec swayidle -w timeout 300 'swaylock -f -c 000000'

### Input configuration

input "1133:49298:Logitech_G203_LIGHTSYNC_Gaming_Mouse" {
    pointer_accel -0.6
}
input "1133:16418:Logitech_Wireless_Mouse_PID:4022" {
    pointer_accel 0.1
}

input type:keyboard {
    xkb_layout us
}
input type:pointer {
    accel_profile flat
    middle_emulation disabled
}
input type:touchpad {
    dwt disabled
    tap enabled
    middle_emulation enabled
}

### Key bindings

# Terminal
bindsym $mod+Shift+Return exec $term

# Close window
bindsym $mod+Shift+c kill

# Menus
bindsym $mod+p exec $menu
bindsym $mod+Shift+p exec $desktop_menu

# Reload sway
bindsym $mod+Shift+r reload

# Exit sway
bindsym $mod+Shift+q $nag -f 'Hack 11' -t warning -m 'Exit sway?' -B 'Yes' 'swaymsg exit'

# Focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right
floating_modifier $mod normal

# Workspace
bindsym $mod+1 workspace number 1
bindsym $mod+2 workspace number 2
bindsym $mod+3 workspace number 3
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7
bindsym $mod+8 workspace number 8
bindsym $mod+9 workspace number 9
bindsym $mod+0 workspace number 10
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
bindsym $mod+Shift+0 move container to workspace number 10

# Layout stuff
bindsym $mod+t layout toggle tabbed split
bindsym $mod+m fullscreen
bindsym $mod+f floating toggle
bindsym --whole-window $mod+BTN_MIDDLE floating toggle
#bindsym $mod+f focus mode_toggle
#bindsym $mod+a focus parent

# Scratchpad
bindsym $mod+Shift+minus move scratchpad
bindsym $mod+minus scratchpad show

# Resizing
mode "resize" {
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

### Styling

# Cursor
seat seat0 xcursor_theme troll 24

# Borders
default_border pixel 2
default_floating_border normal 2

# Colors                border   bg       text     indi     childborder
client.focused          $cl_high $cl_high $cl_back $cl_indi $cl_high
client.focused_inactive $cl_back $cl_back $cl_fore $cl_back $cl_back
client.unfocused        $cl_back $cl_back $cl_fore $cl_back $cl_back
client.urgent           $cl_urge $cl_urge $cl_fore $cl_urge $cl_urge

#client.<class>         <border>  <background> <text> 
#client.focused          "#005577" "#005577"    "#eeeeee"
#client.focused_inactive "#444444" "#222222"    "#bbbbbb"
#client.unfocused        "#444444" "#222222"    "#bbbbbb"

### Environment (https://gitlab.com/slonkazoid/pkgbuild-sway-slonk-git)

env GDK_BACKEND wayland
env QT_WAYLAND_DISABLE_WINDOWDECORATION 1
#env QT_QPA_PLATFORMTHEME qt5ct
env _JAVA_AWT_WM_NONREPARENTING 1
#env XDG_CURRENT_DESKTOP sway # not needed with sway-systemd
env DESKTOP_SESSION sway
env XDG_SESSION_DESKTOP sway
env MOZ_ENABLE_WAYLAND 1

### Exec stuff

# Service bs
exec systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec systemctl --user start sway-session.target
exec /usr/bin/lxqt-policykit-agent

# Autostart
exec dex -ae $DESKTOP_SESSION -s "${XDG_CONFIG_HOME:-$HOME/.config}/autostart"
# Notifications
exec swaync
# Waybar
exec waybar

# Something something wine fix
exec_always xrandr --output $(xrandr | grep -m 1 XWAYLAND | awk '{print $1;}') --primary
# Apply gtk settings to gsettings
exec_always ~/.local/bin/import-gsettings

### Extra binds

# Volume stuff
bindsym --locked XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym --locked XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym --locked XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym --locked XF86AudioMicMute exec pactl set-source-mute @DEFAULT_SOURCE@ toggle
bindsym --locked XF86MonBrightnessDown exec brightnessctl set 5%-
bindsym --locked XF86MonBrightnessUp exec brightnessctl set 5%+
bindsym --locked XF86AudioPlay exec playerctl play-pause
bindsym --locked XF86AudioNext exec playerctl next
bindsym --locked XF86AudioPrev exec playerctl previous

# Screenshot
bindsym Print exec ~/.local/bin/screenshot.sh
bindsym Ctrl+Print exec ~/.local/bin/screenshot.sh area
bindsym Ctrl+Shift+Print exec ~/.local/bin/screenshot.sh freeze
bindsym Shift+Print exec ~/.local/bin/screenshot.sh window

# Screen recording
bindsym $mod+Home exec ~/.local/bin/record.sh
bindsym $mod+End exec killall -s SIGINT wf-recorder

# Notifications
bindsym $mod+n exec swaync-client -t
bindsym $mod+Shift+n exec swaync-client -C

# Other
bindsym $mod+k exec ~/.local/bin/kolourpaint.sh
bindsym Mod4+L exec swaylock -f -c 000000
bindsym Mod4+Shift+L exec swaylock -f -c 000000 && systemctl sleep
bindsym --locked Mod4+Shift+L exec systemctl suspend
#bindsym Mod1+KP_Enter exec ~/.local/bin/obs-savereplay

# No keybind mode
mode "nokeys" {
	bindsym $mod+Escape mode "default"
}
bindsym $mod+Escape mode "nokeys"

include /etc/sway/config.d/*
